﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using CursedItems.Library;
using CursedItems.Library.Controls;

namespace CursedItems.GameScreens {
    public class TitleScreen : BaseGameState {
        #region Field region

        Texture2D backgroundImage;
        LinkLabel startLabel;

        #endregion

        #region Constructor region

        public TitleScreen ( Game game, GameStateManager manager )
            : base(game, manager) {
        }

        #endregion

        #region XNA Method region

        protected override void LoadContent () {
            ContentManager Content = GameRef.Content;
            backgroundImage = Content.Load<Texture2D>(@"Backgrounds/titlescreen");
            int posX = backgroundImage.Bounds.Center.X;
            base.LoadContent();
            startLabel = new LinkLabel();
            startLabel.Text = "Press Enter to begin";
            startLabel.Size = startLabel.SpriteFont.MeasureString(startLabel.Text);
            startLabel.Position = new Vector2(posX - (startLabel.Size.X/2),600);
            startLabel.Color = Color.Blue;
            startLabel.TabStop = true;
            startLabel.HasFocus = true;
            startLabel.Selected += new EventHandler(startLabel_Selected);
            ControlManager.Add(startLabel);
        }

        public override void Update ( GameTime gameTime ) {
            ControlManager.Update(gameTime, PlayerIndex.One);
            base.Update(gameTime);

        }

        public override void Draw ( GameTime gameTime ) {

            GameRef.SpriteBatch.Begin();
            base.Draw(gameTime);
            GameRef.SpriteBatch.Draw(backgroundImage, GameRef.ScreenRectangle, Color.White);
            ControlManager.Draw(GameRef.SpriteBatch);
            GameRef.SpriteBatch.End();

        }

        #endregion

        #region Title Screen Methods
        private void startLabel_Selected ( object sender, EventArgs e ) {
            StateManager.PushState(GameRef.StartMenuScreen);
        }
        #endregion
    }
}