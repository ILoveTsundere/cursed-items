CursedItems
===========

Cursed Items RPG

   The 'cursed items' are objects that grant the bearer an incredible powers at a relative cost. These items come in all shapes and sizes and once an item contacts a person it is bound to them until death.

  The neighboring countries Istriela and Boburg have been fighting over such items for decades. Istriela promotes the use of the cursed items, since they have given perks such as healing, stronger troops, easier construction, etc. whereas Boburg argues that such advancements come at a deep cost, since it lowers the mental health of the population and creates an unstable society. After a decades of fighting, with both countries economically and socially devastated by the war, an implicit ceasefire arrived.

   It has been 20 years since the cursed war that consumed the vast continent of Beachlea had come to a halt. Boburg's methods of dealing with Bearers have increased.


Using the RPG tutorial found on this site http://xnagpa.net/xna4rpg.php


(Obviously with my own litle twist)
